if(window.SimpleSlide){

    new SimpleSlide({
        slide: "quote", // nome do atributo data-slide="principal"
        time: 5000, // tempo de transição dos slides
    });

    new SimpleSlide({
        slide: "portfolio", 
        time: 5000,
        nav:true
    });

}

if(window.SimpleAnime){
    
    new SimpleAnime();
}

if(window.SimpleForm){
    new SimpleForm({
        form: ".formphp", //seletor do formulário
        button: "#enviar", //seletor do botão
        sucesso: "<div id='form-sucesso'><h1>Seu email foi enviado!<h1><p>em breve entramos em contato com você.</p></div>",
        erro: "<div id='form-erro'><h2>ERRO NO ENVIO.<h2><p>Ocorreu um erro, tente novamente.</p></div>" //msg de erro
        // sucesso: "<div id='form-sucesso'><h1>Seu email foi enviado!<h1><p>em breve entramos em contato com você.</p></div>",

    });
}
